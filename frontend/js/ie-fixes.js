if(document.documentMode || /Edge/.test(navigator.userAgent)) {
    $(".portfolio-image a").each(function () {
        var $container = $(this);
        var imgObj = $container.find("img");
        var imgUrl = imgObj.prop("src");
        if (imgUrl) {
            $container.css("backgroundImage", 'url(' + imgUrl + ')').addClass("custom-object-fit");
            imgObj.remove();
        }
    });
}